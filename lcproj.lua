local lcproj = {}
local util = require("util")
local sql = require("lsqlite3")
local inspect = require("inspect")

--Return a table containing id. name, & description for a project
function lcproj.getProject(project)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT id,name,desc FROM projects where name = :projname")
   local info = {}
   stmt:bind_names({projname = project})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   --print(inspect(info[1]))
   return info[1]
end

--Return a project description from the db
function lcproj.getProj(name)
   local db = sql.open("lc.db")
   local info = util.sql_select(db, "projects", {"name", "desc"}, "=", {"name"}, {name})
   return info[1]
end

--Return a created amd updated times for a post
function lcproj.getUpdated(name)
   local db = sql.open("lc.db")
   local info = util.sql_select(db, "projects", {"created_on", "updated_on"}, "=", {"name"}, {name})
   return info[1]
end

--Return a table of all projects
function lcproj.getArchive()
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT name FROM projects")
   local info = {}
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

return lcproj
