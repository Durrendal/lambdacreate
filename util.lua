local util = {}
local lume = require("lume")
local sql = require("lsqlite3")
local mime = require("mime")
local cipher = require("openssl.cipher")
local rand = require("openssl.rand")
local inspect = require("inspect")

function util.conf(config)
   local f = io.open(config, "r")
   local content = lume.deserialize(f:read("*a"))
   return content
end

function util.split(val, check)
  if (check == nil) then
    local check = "%s"
  else
  end
  local t = {}
  for str in string.gmatch(val, ("([^" .. check .. "]+)")) do
    table.insert(t, str)
  end
  return t
end

function util.exists(f)
  local f = io.open(f, "r")
  if f then
    return f:close()
  else
    return false
  end
end

function util.dir_exists(dir)
  local stat, err, code = os.rename(dir, dir)
  if (stat == nil) then
    if (err == 13) then
      return true
    else
    end
    return false
  else
  end
  return true
end

function util.bin_to_hex(string)
   return (string:gsub('.', function(char)
						  return string.format('%02X', string.byte(char))
   end))
end

function util.hex_to_bin(string)
   return string:gsub('..', function(chars)
						 return string.char(tonumber(chars, 16))
   end)
end

function util.encrypt(string, ciphertype, cipherkey)
   local cipher0 = cipher.new(ciphertype)
   local iv = rand.bytes(16)
   return util.bin_to_hex(iv .. cipher0:encrypt(cipherkey, iv):final(string))
end

function util.decrypt(encstr, ctype, key)
   local cipher0 = cipher.new(ctype)
   local iv = util.hex_to_bin(encstr:sub(0 + 1, 31 + 1))
   local string = util.hex_to_bin(encstr:sub(32 + 1))
   return cipher0:decrypt(key, iv):final(string)
end

function util.sql_modify(db, id, table, coltbl, valtbl)
  if ((type(coltbl) ~= "table") or (type(valtbl) ~= "table")) then
    do end (io.stderr):write("Invalid table passed to sql/modify\n")
    os.exit()
  else
  end
  local update = ""
  for k, v in ipairs(coltbl) do
    update = (update .. ("'" .. v .. "' = '" .. string.gsub(valtbl[v], "'", "''") .. "',"))
  end
  local stmt = db:prepare(("UPDATE " .. table .. " SET " .. update:sub(1, -2) .. " WHERE id = " .. id .. ";"))
  stmt:step()
  return stmt:finalize()
end

function util.sql_select(db, sqltbl, coltbl, fltrtyp, fltrtbl, valtbl)
  if ((type(coltbl) ~= "table") or (type(fltrtbl) ~= "table") or (type(valtbl) ~= "table")) then
    do end (io.stderr):write("Invalid table passed to sql/select.\n")
    os.exit()
  elseif ((fltrtyp ~= "LIKE") and (fltrtyp ~= "=") and (fltrtyp ~= "!=")) then
    do end (io.stderr):write((fltrtyp .. " is an invalid filter type. Try LIKE, =, or !=.\n"))
    os.exit()
  else
  end
  local selection = ""
  local filter = ""
  for k, v in ipairs(coltbl) do
    selection = (selection .. ("\"" .. v .. "\","))
  end
  if (#fltrtbl > 1) then
    for k, v in ipairs(fltrtbl) do
      filter = (filter .. ("\"" .. valtbl[k] .. "\","))
    end
  else
    local function _16_()
      if (fltrtyp == "LIKE") then
        return "%"
      else
        return ""
      end
    end
    local function _17_()
      if (fltrtyp == "LIKE") then
        return "%"
      else
        return ""
      end
    end
    filter = (filter .. (fltrtbl[1] .. " " .. fltrtyp .. " '" .. _16_() .. valtbl[1] .. _17_() .. "'"))
  end
  local stmt = db:prepare(("SELECT " .. selection:sub(1, -2) .. " FROM " .. sqltbl .. " WHERE " .. filter .. ";"))
  local retval = {}
  for row in stmt:nrows() do
    table.insert(retval, row)
  end
  stmt:finalize()
  return retval
end

return util
