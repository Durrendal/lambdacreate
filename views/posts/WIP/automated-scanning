<h2>Malpeza</h2>
<h3>Not Big, But Certainly Fun &middot May 5th, 2023</h3>

<p>This one, like Malpeza, is short and sweet. I wrote a tiny IRC bot in a fever pitch over the weekend, right before heading out on a work trip. It doesn't do too much, but that's okay, it doesn't need to. See the magic is that with a little bit of Lua and some shell scripting I was able to build a tiny portable network mapping system! "Oh I bet this is just some wrapper around nmap or tcpdump or something" you might be thinking to yourself, and you wouldn't be wrong to think that either. The actual network mapping functionality is dead simple, but the fact that such a simple solution will save me 4ish hours on this trip alone is pretty exciting!</p>

#explain what I'm doing and why it saves time

<h3>The Device</h3>

<p>First lets talk about the device itself. I'm using a Dell Wyse 3040, it's marketed as a thin client, is about the size of a raspberry pi in a case, and sports a 4 core atom cpu with a couple gigs of ram, and an itty bitty 8gb emmc flash for storage. Not a whole lot, but it also has an rj45 port, which is really what I'm after. And the size! Seriously, this thing is TINY. Here's the droid for scale.</p>

<img>

<p>This is a delightful form factor, it fits easily into any carry on, has enough oomph to run LXD if needed. And if you compare it to an Intel NUC it's maybe 1/4 the weight and size easy. This makes it easy to drop on top of a switch somewhere discreet and out the way, without worrying too much if it'll look out of place. Which is exactly what this entire thing is for. This little guy runs is setup to run a big old network mapping script once it boots up, so all I need to do with it is find a port, plug it in, and walk away.</p>

#explain what script does
<div class="codeSnippet">
  <pre><code>
#!/bin/ash
#set -x
stamp=$(date +%Y%m%d%H:%M:%S)
log_dir=/var/lan-map

#Gather and calculate network information
mon_intf=wlan0
lan_raw=$(ip addr show dev $mon_intf | grep inet | awk '{print $2}' | head -n1)
cidr="$(ipcalc -n $lan_raw | awk -F'=' '{ print $2 }')/$(ipcalc -p $lan_raw | awk -F'=' '{ print $2 }')"
netmask=$(ipcalc -m $lan_raw | awk -F'=' '{ print $2 }')
nm_subnet=$(ipcalc -n $lan_raw | awk -F'=' '{ print $2 }')
broadcast=$(ipcalc -b $lan_raw | awk -F'=' '{ print $2 }')
gateway=$(echo "$nm_subnet" | awk -F'.' '{ print $1"."$2"."$3"."1 }')

ensure_log_dir () {
    if [ ! -d $log_dir ]; then
	mkdir $log_dir
    fi
    
    if [ ! -d $log_dir/$stamp ]; then
	mkdir $log_dir/$stamp
    fi	
}

net_cap () {
    len=600
    tcpdump -i $mon_intf -G $len -F $log_dir/$stamp/$lan.pcap
}

nmap_cap () {
    nmap -sP $cidr > $log_dir/$stamp/nmap-sp.txt
    nmap -sL $cidr > $log_dir/$stamp/nmap-sl.txt
    printf "===[ARP Info]===\n"
    arp -a >> $log_dir/$stamp/ip_info.txt
    nmap -p1-65535 $cidr > $log_dir/$stamp/nmap-ports.txt
}

ip_info () {
    printf "===[Calculated Network Info]===
IP Reported: $lan_raw
CIDR: $cidr
Netmask: $netmask
Subnet: $nm_subnet
Broadcast: $broadcast
Gateway: $gateway
" > $log_dir/$stamp/ip_info.txt

    printf "===[IP Info]===\n" >> $log_dir/$stamp/ip_info.txt
    ip link >> $log_dir/$stamp/ip_info.txt
    printf ""
    ip addr >> $log_dir/$stamp/ip_info.txt
    printf ""
    ip route >> $log_dir/$stamp/ip_info.txt

    printf "===[Traceroute]===\n" >> $log_dir/$stamp/ip_info.txt
    traceroute $gateway >> $log_dir/$stamp/ip_info.txt
    printf ""
    traceroute 1.1.1.1 >> $log_dir/$stamp/ip_info.txt
    printf ""
    traceroute 8.8.8.8 >> $log_dir/$stamp/ip_info.txt
    printf ""
    traceroute 9.9.9.9 >> $log_dir/$stamp/ip_info.txt
}

finished () {
    touch /tmp/finished
}

ensure_log_dir
ip_info
nmap_cap
net_cap
finished
  </code></pre>
</div>

<h3>The IRC Bot</h3>
