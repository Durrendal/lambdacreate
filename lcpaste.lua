local lcpaste = {}
local sql = require("lsqlite3")

function lcpaste.save(cont, fname)
   --Save file to /tmp/lua_XXXXXX
   local path = os.tmpname()
   local XXXXXX = string.gsub(path, "/tmp/lua_", "")
   local f = io.open(path, "w")
   io.output(f)
   io.write(cont)
   io.close(f)

   --TODO: Fix this cause it doesn't work~
   --Archive paste file
   --local db = sql.open("lc.db")
   --local stmt = db:prepare("INSERT INTO paste (filename, content) VALUES (:filename, :content)")
   --stmt:bind_names({filename = fname, content = cont})
   --stmt:step()
   --stmt:finalize()
   
   --Move pasted file into paste directory
   local statpath = "static/paste/" .. fname
   os.execute("cp " .. path .. " " .. statpath)
   return fname
end

function lcpaste.getArchive()
   local files = io.popen('ls static/paste')
   local archive = {}
   for f in files:lines() do
	  table.insert(archive, f)
   end
   return archive
end

--Return a table of all paste ids and their titles
--function lcpaste.getArchive()
--   local db = sql.open("lc.db")
--   local stmt = db:prepare("SELECT id,title FROM paste")
--   local info = {}
--   for row in stmt:nrows() do
--	  table.insert(info, row)
--   end
--   stmt:finalize()
--   return info
--end

return lcpaste
