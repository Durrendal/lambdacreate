local lcpost = {}
local util = require("util")
local sql = require("lsqlite3")
local inspect = require("inspect")

--Return a table containing id, title, description, and pubdate of post
function lcpost.getPost(id)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT id,title,desc,pubdate FROM posts WHERE id = :idnum")
   local info = {}
   stmt:bind_names({idnum = id})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   --print(inspect(info[1]))
   return info[1]
end

--Return a created amd updated times for a post
function lcpost.getUpdated(id)
   local db = sql.open("lc.db")
   local info = util.sql_select(db, "posts", {"created_on", "updated_on"}, "=", {"id"}, {id})
   return info[1]
end

--Return a table of all post ids and their titles
function lcpost.getArchive()
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT id,title FROM posts")
   local info = {}
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

--Return a table of the last X records
function lcpost.getLast(num)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT id,title FROM posts ORDER BY id DESC LIMIT :limit")
   local info = {}
   stmt:bind_names({limit = num})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

--Return the latest post # from the db
function lcpost.getLatest()
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT count(id) FROM posts")
   local info = {}
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   --print(info[1]["count(id)"])
   return info[1]["count(id)"]
end

return lcpost
