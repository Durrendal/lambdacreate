local lcpod = {}
local util = require("util")
local sql = require("lsqlite3")
local inspect = require("inspect")

--Return a table containing the episode #, title, description, and publish date
function lcpod.getEpisode(show, episode)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT episode,title,desc,pubdate FROM episodes WHERE show = :showname AND episode = :episodenum")
   local info = {}
   stmt:bind_names({showname = show, episodenum = episode})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   --print(inspect(info[1]))
   return info[1]
end

function lcpod.getShows()
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT name,subtitle,desc FROM shows")
   local info = {}
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

--Return a show name from the db
function lcpod.getShow(name)
   local db = sql.open("lc.db")
   local info = util.sql_select(db, "shows", {"name", "subtitle", "desc"}, "=", {"name"}, {name})
   return info[1]
end

--Return a created amd updated times for a post
function lcpod.getUpdated(show, id)
   local db = sql.open("lc.db")
   local info = util.sql_select(db, "episodes", {"created_on", "updated_on"}, "=", {"id"}, {id})
   return info[1]
end

--Return a table of all episode #s and their titles for a show
function lcpod.getArchive(show)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT episode,title FROM episodes WHERE show = :showname")
   local info = {}
   stmt:bind_names({showname = show})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

--Return a table of the last X records
function lcpod.getLast(num, show)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT episode,title FROM episodes WHERE show = :showname ORDER BY episode DESC LIMIT :limit")
   local info = {}
   stmt:bind_names({showname = show, limit = num})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   return info
end

--Return the latest episode # from the db
function lcpod.getLatest(show)
   local db = sql.open("lc.db")
   local stmt = db:prepare("SELECT count(episode) FROM episodes WHERE show = :showname")
   local info = {}
   stmt:bind_names({showname = show})
   for row in stmt:nrows() do
	  table.insert(info, row)
   end
   stmt:finalize()
   --print(info[1]["count(episode)"])
   return info[1]["count(episode)"]
end

return lcpod
