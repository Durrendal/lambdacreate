#!/bin/ash
set -x
orig=$(ls | grep -o 'orig')

if [ "$orig" == "" ]; then
	for f in $(ls); do
		root=$(echo $f | awk -F'.' '{print $1}')
		type=$(echo $f | awk -F'.' '{print $2}')
		echo "Converting $f.."
		mv $f $root-orig.$type
		convert -resize 45% $root-orig.$type $f
	done
else
	for f in $(ls | grep 'orig'); do
		root=$(echo $f | awk -F'.' '{print $1}')
		type=$(echo $f | awk -F'.' '{print $2}')
		export=$(echo $f | sed 's/-orig//')
		echo "Converting $f.."
		echo $export
		convert -resize 45% $f $export
	done
fi
