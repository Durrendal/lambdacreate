#!/bin/ash
deps="lume lsqlite3 inspect openssl argparse"

for dep in $deps; do
	luarocks-5.1 install $dep
done
