local posts = {}
--local etlua = require("etlua")
--local config = require("lapis.config").get()

posts.archive = {
   {
      title = "About Lambda Create",
      fname = "1",
      desc = "Over-Engineering at it's finest",
      pdate = "2020-04-07"
   },
   {
      title = "Over-Engineering a Blog: Part 1",
      fname = "2",
      desc = "A technical overview of how LambdaCreate works",
      pdate = "2020-04-07"
   },
   {
      title = "Over-Engineering a Blog: Part 2",
      fname = "3",
      desc = "A technical overview of how LambdaCreate works",
      pdate = "2020-04-08"
   },
   {
      title = "My Docker Workflow",
      fname = "4",
      desc = "Why should I even leave Emacs?",
      pdate = "2020-04-11"
   },
   {
      title = "OpenRC & Open Source",
      fname = "5",
      desc = "The shoulders of giants",
      pdate = "2020-04-14"
   },
   {
      title = "toAPK chez-scheme",
      fname = "6",
      desc = "Porting Cisco Chez Scheme with toAPK",
      pdate = "2020-04-16"
   },
   {
	  title = "Tying up loose ends",
	  fname = "7",
	  desc = "Prepping toAPK for release, and life updates",
	  pdate = "2020-04-22"
   },
   {
	  title = "Mobile Workflow",
	  fname = "8",
	  desc = "Managing development on the go",
	  pdate = "2020-05-01"
   },
   {
	  title = "toAPK -v template",
	  fname = "9",
	  desc = "Adding VOID Template Support to toAPK",
	  pdate = "2020-05-09"
   },
   {
	  title = "Fennel Confusion",
	  fname = "10",
	  desc = "Sorting Through Some Personal Confusion",
	  pdate = "2020-05-18"
   },
   {
	  title = "Emulating aarch64",
	  fname = "11",
	  desc = "Finally building SBCL on aarch64 arm platforms!",
	  pdate = "2020-06-04"
   },
   {
	  title = "Macros in Fennel",
	  fname = "12",
	  desc = "Automating the fun stuff",
	  pdate = "2020-06-09"
   },
   {
	  title = "Did I Make Enough Static?",
	  fname = "13",
	  desc = "How I learned to stop worrying, and love the .exe.",
	  pdate = "2020-07-01"
   },
   {
	  title = "Building an Alpine NAS",
	  fname = "14",
	  desc = "Abandoning RHEL's Ship",
	  pdate = "2020-07-16"
   },
   {
	  title = "Striving Towards Happiness",
	  fname = "15",
	  desc = "Reflections on Life",
	  pdate = "2020-08-03"
   },
   {
	  title = "Mobile Workflow: Part 2",
	  fname = "16",
	  desc = "A Nokia N900 Really is my Daily Driver!",
	  pdate = "2020-08-28"
   },
   {
	  title = "Decentralized Collaboration",
	  fname = "17",
	  desc = "Old School Collaboration",
	  pdate = "2020-09-16"
   },
   {
	  title = "Building Blocks",
	  fname = "18",
	  desc = "The Nature of Progression",
	  pdate = "2020-09-24"
   },
   {
	  title = "Lisp, Make, & Esper",
	  fname = "19",
	  desc = "Homogenous Packaging for Heterogenous Environments",
	  pdate = "2020-10-07"
   },
   {
	  title = "MikroTik Maplite Magic",
	  fname = "20",
	  desc = "Creating the perfect travel router",
	  pdate = "2020-11-20"
   },
   {
	  title = "Low Spec Computers",
	  fname = "21",
	  desc = "Doing real work with minimal resources",
	  pdate = "2020-12-04"
   },
   {
	  title = "This Distro is Not For You",
	  fname = "22",
	  desc = "Ignoring Stern Warnings, and Tinkering with Plan9 Anyways",
	  pdate = "2021-01-15"
   },
   {
	  title = "Very Little Indeed",
	  fname = "23",
	  desc = "Digital age minimalism",
	  pdate = "2021-04-19"
   },
   {
	  title = "Thinking Like a SysAdmin",
	  fname = "24",
	  desc = "Taking RTFM to heart",
	  pdate = "2021-06-22"
   },
   {
	  title = "OFTC",
	  fname = "25",
	  desc = "x509 Cert Auth with Weechat",
	  pdate = "2021-07-08"
   },
   {
	  title = "The Old Computer Challenge",
	  fname = "26",
	  desc = "Showing Abandoned Hardware a Bit of Love",
	  pdate = "2021-08-24"
   },
   {
	  title = "Alpine Workstations",
	  fname = "27",
	  desc = "Getting Things Working",
	  pdate = "2021-09-27"
   },
   {
	  title = "Modifying boot.wim",
	  fname = "28",
	  desc = "And using it to bypass Win11 installation checks",
	  pdate = "2021-11-06"
   },
   {
	  title = "LFCE",
	  fname= "29",
	  desc = "Lessons Learned",
	  pdate = "2021-12-21"
   },
   {
	  title = "2021 Review",
	  fname = "30",
	  desc = "Self Reflections and Goal Settings",
	  pdate = "2021-12-31"
   },
   {
	  title = "Why tkts?",
	  fname = "31",
	  desc = "Writing a ticketing system from scratch",
	  pdate = "2022-02-10",
   },
   {
	  title = "The Casa Repo Project",
	  fname = "32",
	  desc = "Tribulations with Homelab CI/CD",
	  pdate = "2022-02-12"
   },
   {
	  title = "Alpine NAS V2",
	  fname = "33",
	  desc = "Upgrading to Enterprise Hardware",
	  pdate = "2022-04-06"
   },
   {
	  title = "The Infamous Droid",
	  fname = "34",
	  desc = "Revisiting My Mobile Workflow",
	  pdate = "2022-04-24"
   },
}

return posts
