local projects = {}

projects.info = {
   "Atentu",
   "Esper",
   "toAPK",
   "tkts"
}
--   "Dockerhub",

projects.docker = {
   "tiddlydocker",
   "mrbuildit",
   "minedock",
   "lighty",
   "fnl-mingw-compile",
   "apkbuild-lint",
   "alpinelab"
}

return projects
