#!/usr/bin/lua
local comments = require("comments")
local posts = require("posts")
local projects = require("projects")
local handler = require("handler")

function main (call)
   if call == "--rss" then
	  print(handler.makefeed())
   end
end

main(...)
