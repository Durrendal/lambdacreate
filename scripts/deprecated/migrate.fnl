#!/usr/bin/fennel
;;Migrate from .lua files to sqlite3 db
(local posts (require "posts"))
(local projects (require "projects"))
(local inspect (require "inspect"))

;;(print (inspect posts))
;;(print (inspect projects))

(each [k v (pairs posts.archive)]
  (do
    (print (inspect v))
    (let
        [title v.title
         desc v.desc
         pubdate v.pdate]
      (os.execute (.. "lcm post -a \"" title "\" \"" desc "\" " pubdate)))))

(each [k v (pairs projects.info)]
  (do
    (let
        [name v
         desc v
         git (.. "https://gitlab.com/durrendal/" name ".git")]
      (os.execute (.. "lcm proj -a \"" name "\" \"" desc "\" \"" git "\"")))))

(os.execute "lcm show -a 'droidcast' 'A podcast produced, edited, and published entirely on an old Motorola Droid4 running Alpine Linux!' 'nil'")
(os.execute "lcm show -a 'lambdacast' 'Same greate lambdacreate content, not in audio format!' 'nil'")
(os.execute "lcm rss -b > /lapis/static/feed.rss")
