#!/usr/bin/fennel
;; To: Will Sinatra
;; From: Jesse Laprade <3
;; Description: A script so all of your cool internet friends can keep
;;              up with your blog posts!

(local posts (require :posts))

(local title "(lambda (x) (create x))")        ;; Change me
(local url "https://lambdacreate.com")          ;; Do not add a slash to the end
(local description "A blog held together entirely by lua and coffee.")      ;; Change me
(local rss-file "feed.rss")                    ;; Do not edit
(local url-rss (.. url "/static/" rss-file))
(local output-file (.. "static/" rss-file))
(local posts-location (.. url "/posts/"))      ;; 1, 2, 3 get appended, so the slash is required

(local rss-header
       (string.format
        (.. "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
            "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n"
            "\n"
            "  <channel>\n"
            "    <title>%s</title>\n"
            "    <link>%s</link>\n"
            "    <description>%s</description>\n"
            "    <atom:link href=\"%s\" rel=\"self\" type=\"application/rss+xml\" />\n")
        title
        url
        description
        url-rss))

(local rss-item
       (.. "\n"
           "  <item>\n"
           "    <title><![CDATA[%s]]></title>\n"
           "    <description><![CDATA[%s]]></description>\n"
           "    <link>%s</link>\n"
           "    <guid>%s</guid>\n"
           "    <pubDate>%s</pubDate>\n"
           "  </item>\n"))

(local rss-footer
       (.. "  </channel>\n"
           "</rss>\n"))

(fn shell->sequence [command]
  (let [seq []]
    (with-open [file-handle (io.popen command :r)]
      (each [i (file-handle:lines)]
        (table.insert seq i)))
    seq))

(fn date-convert [date]
  (. (shell->sequence (string.format "date -Rd'%s'" date)) 1))

(fn file-write [file data mode]
  (with-open [file-out (io.open file mode)]
    (file-out:write data)))

(fn file-write-rss-items [file]
  (let [archive posts.archive]
    (each [post _ (ipairs archive)]
      (let [title       (. archive post :title)
            description (. archive post :desc)
            link        (.. posts-location (. archive post :fname))
            guid        link
            date        (date-convert (. archive post :pdate))
            item        (string.format rss-item
                                       title
                                       description
                                       link
                                       guid
                                       date)]
        (file-write file item :a)))))

(fn main []
  (print (string.format "Generating RSS feed at %s ..." output-file))
  (file-write output-file rss-header :w)
  (file-write-rss-items output-file)
  (file-write output-file rss-footer :a))

(main)
