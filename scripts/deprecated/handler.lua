local handler = {}
local posts = require("posts")

function handler.getNKey()
   local new = #posts.archive
   return new
end

function handler.getNewest()
   local new = #posts.archive
   local view = "views.posts." .. new
   return view
end

function handler.getUpdated()
   local new = #posts.archive
   local last = posts.archive[new]["pdate"]
   return last
end

function handler.rssdate(str)
   local convert = io.popen("date -u -R -d " .. str)
   --strftime("%a, %d %b %Y %T %z")
   local rc = convert:read("*a")
   local rssstr = string.gsub(rc, "\n", "")
   return rssstr
end

function handler.makeRSS(key)
   local title = posts.archive[key]["title"]
   local link = "https://lambdacreate.com/posts/" .. posts.archive[key]["fname"]
   local pudDate = posts.rssdate(posts.archive[key]["pdate"])
   local desc = posts.archive[key]["desc"]

   return "<item>\n" .. "<title>" .. title .. "</title>\n" .. "<link>" .. link .. "</link>\n" .. "<pubDate>" .. pubDate .. "</pubDate>\n" .. "<description>" .. desc .. "</description>\n" .. "</item>\n"
end

function handler.makefeed()
   local rsstbl = {'<?xml version="1.0" encoding="utf-8"?>\n<rss version="2.0">\n<channel>\n'}
   for i=1, #posts.archive do
	  local val = #posts.archive + 1 - i
      local title = string.gsub(posts.archive[val]["title"], "&", "and")
      local link = "http://www.lambdacreate.com/posts/" .. posts.archive[val]["fname"]
      local pubDate = handler.rssdate(posts.archive[val]["pdate"])
      local desc = posts.archive[val]["desc"]

      table.insert(rsstbl, #rsstbl + 1, "\n<item>\n" .. "<title>" .. title .. "</title>\n" .. "<link>" .. link .. "</link>\n\
" .. "<pubDate>" .. pubDate .. "</pubDate>\n" .. "<description>" .. desc .. "</description>\n" .. "</item>\n")
   end

   table.insert(rsstbl, #rsstbl + 1, "\n</channel>\n</rss>")
   return unpack(rsstbl)
end

return handler
