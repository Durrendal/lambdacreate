#!/usr/bin/fennel
;;===== Deps =====
(local lume (require :lume))
(local arg (require :argparse))
(local sql (require :lsqlite3))
(local mime (require :mime))
(local cipher (require "openssl.cipher"))
(local rand (require "openssl.rand"))
(local inspect (require :inspect))

;;===== Global Vars =====
(global ver {"db" 0})

(var conf {"db_loc" "/lc.db"
           "editor" "mg"
           "cipher" "cipher.conf"
           "lapis" "/lapis"
           "cipher" {"type" "aes-256-cbc"
                     "key" ""}})

;;===== DB Schema =====
(global schematbl {"version"
                   "CREATE TABLE IF NOT EXISTS version (
db_ver INTEGER NOT NULL);"
                   "posts"
                   "CREATE TABLE IF NOT EXISTS posts (
id INTEGER PRIMARY KEY,
title TEXT NOT NULL,
desc TEXT NOT NULL,
pubdate TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "projects"
                   "CREATE TABLE IF NOT EXISTS projects (
id INTEGER PRIMARY KEY,
name TEXT UNIQUE NOT NULL,
desc TEXT NOT NULL,
git TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "shows"
                   "CREATE TABLE IF NOT EXISTS shows (
id INTEGER PRIMARY KEY,
name TEXT UNIQUE NOT NULL,
subtitle TEXT NOT NULL,
desc TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "episodes"
                   "CREATE TABLE IF NOT EXISTS episodes (
id INTEGER PRIMARY KEY,
show TEXT NOT NULL,
episode INTEGER NOT NULL,
title TEXT NOT NULL,
desc TEXT NOT NULL,
pubdate TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (show) REFERENCES shows (name) ON DELETE CASCADE);"
                   "apikeys"
                   "CREATE TABLE IF NOT EXISTS apikeys (
id INTEGER PRIMARY KEY,
key TEXT UNIQUE NOT NULL,
user TEXT UNIQUE NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "paste"
                   "CREATE TABLE IF NOT EXISTS paste (
id INTEGER PRIMARY KEY,
filename TEXT NOT NULL,
content TEXT NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_on DATETIME DEFAULT CURRENT_TIMESTAMP);"})

(fn db/defnschema []
  "Create database schema"
  (let [db (sql.open (.. conf.lapis conf.db_loc))]
    (db:exec schematbl.version)
    (db:exec (.. "INSERT INTO version (db_ver) VALUES (" ver.db ");"))
    (db:exec schematbl.posts)
    (db:exec schematbl.projects)
    (db:exec schematbl.shows)
    (db:exec schematbl.episodes)
    (db:exec schematbl.apikeys)
    (db:exec schematbl.paste)
    (db:close)))

;;Not what we're doing long term, but will be OK for testing.
(fn db/migrate []
  "Migrate between schema versions"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    ;;if db_ver != ver.db defined in schema, migrate
    (each [row (db:nrows "SELECT db_ver from version;")]
      (if (~= (. row "db_ver") ver.db)
          (os.execute (.. "mv " (.. conf.lapis conf.db_loc) " "  "lc_" (. row "db_ver") ".db"))))))
;;TODO: Figure out a way to migrate data when this occurs.

;;===== util =====
(fn util/split [val check]
  "Split string on delim and return as table"
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)

(fn util/exists? [f]
  "Check if file exists"
  (local f? (io.open f "r"))
  (if f?
      (f?:close)
      false))

(fn util/dir_exists? [dir]
  (let
      [(stat err code) (os.rename dir dir)]
    (when (= stat nil)
      (when (= err 13)
        (lua "return true"))
      (lua "return false"))
    true))

(fn util/sizeof [file]
  (with-open [f (io.open file "r")]
    (f:seek "end")))

;;(util/rsstime "2022-06-30") -> Thu, 30 Jun 2022 00:00:00 +0000
(fn util/rsstime [str]
  (if (= str nil)
      (os.date "%a, %d %b %Y %H:%M:%S +0000")
      (let
          [dat (util/split str "-")
           time {"year" (. dat 1) "month" (. dat 2) "day" (. dat 3)}]
        (os.date "%a, %d %b %Y %H:%M:%S +0000" (os.time time)))))

;;(util/template {1 "Templates are just strings with {variables}" :variables "Something substituted"})
(fn util/template [str vars]
  (when (not vars)
    (let
        [vars str
         str (. vars 1)]
      (string.gsub str "({([^}]+)})"
                   (fn [whole i]
                     (or (. vars i) whole))))))
      
(fn util/bin->hex [string]
  (string:gsub "."
               (fn [char] (string.format "%02X" (string.byte char)))))

(fn util/hex->bin [string]
  (string:gsub ".."
               (fn [chars] (string.char (tonumber chars 16)))))

(fn util/encrypt [string]
  (let
      [cipher (cipher.new conf.cipher.type)
       iv (rand.bytes 16)]
    (util/bin->hex
     (.. iv
         (: (cipher:encrypt conf.cipher.key iv)
            :final string)))))

(fn util/decrypt [encstr]
  (let
      [cipher (cipher.new conf.cipher.type)
       iv (util/hex->bin (encstr:sub (+ 0 1) (+ 31 1)))
       string (util/hex->bin (encstr:sub (+ 32 1)))]
    (: (cipher:decrypt conf.cipher.key iv)
       :final string)))

(fn util/conf [config]
  "Load configuration"
  ;;if conf file exists
  (if (util/exists? config)
      ;;load config & verify all settings exist
      (with-open [f (io.open config "r")]
        (set conf (lume.deserialize (f:read "*a"))))
      ;;else, create a new one with defaults
      (with-open [f (io.open config "w")]
        (f:write (inspect conf))))
  ;;if lc.db exists, check if migration is needed or not, if it doesn't exist create
  (if (util/exists? (.. conf.lapis conf.db_loc))
      (db/migrate)
      (db/defnschema)))

;;===== sql =====
;;(sql/modify (sql.open (.. conf.lapis conf.db_loc)) 1 "posts" ["title" "desc"] {"title" "YipiYipiYay" "desc" "whooo~"})
(fn sql/modify [db id table coltbl valtbl]
  "Update existing columns with specified values"
  (if (or (~= (type coltbl) "table") (~= (type valtbl) "table"))
      (do
        (io.stderr:write "Invalid table passed to sql/modify\n")
        (os.exit)))
  (var update "")
  ;;collate coltbl values with valtbl values using coltbl as keys
  (each [k v (ipairs coltbl)]
    (set update (.. update (.. "'" v "' = '" (-> (. valtbl v)
                                                 (string.gsub "'" "''")) "',"))))
  ;;:sub 1 -2 here removes the last trailing ,
  (let
      [stmt (db:prepare (.. "UPDATE " table " SET " (update:sub 1 -2) " WHERE id = " id ";"))]
    (stmt:step)
    (stmt:finalize)))

;;(sql/select (sql.open (.. conf.lapis conf.db_loc)) "client" ["email" "role"] "LIKE" ["name"] ["will"]) <- select where name like will
;;(sql/select (sql.open (.. conf.lapis conf.db_loc)) "client" ["email" "role"] "LIKE" ["name"] [""]) <- select anything
;;(sql/select (sql.open (.. conf.lapis conf.db_loc)) "tickets" ["client" "title"] "=" ["id"] [10]) <- select where X = Y
;;(sql/select (sql.open (.. conf.lapis conf.db_loc)) "company" ["name"] "!=" ["abbrv"] ["LC"]) <- select where X not like Y
(fn sql/select [db sqltbl coltbl fltrtyp fltrtbl valtbl]
  (if (or (~= (type coltbl) "table") (~= (type fltrtbl) "table") (~= (type valtbl) "table"))
      (do
        (io.stderr:write "Invalid table passed to sql/select.\n")
        (os.exit))
      (and (~= fltrtyp "LIKE") (~= fltrtyp "=") (~= fltrtyp "!="))
      (do
        (io.stderr:write (.. fltrtyp " is an invalid filter type. Try LIKE, =, or !=.\n"))
        (os.exit)))
  (var selection "")
  (var filter "")
  (each [k v (ipairs coltbl)]
    (set selection (.. selection (.. "\"" v "\","))))
  (if (> (length fltrtbl) 1)
      (each [k v (ipairs fltrtbl)]
        (set filter (.. filter (.. "\"" (. valtbl k) "\","))))
      (set filter (.. filter (.. (. fltrtbl 1) " " fltrtyp " '" (if (= fltrtyp "LIKE") "%" "") (. valtbl 1) (if (= fltrtyp "LIKE") "%" "") "'"))))
  ;;:sub 1 -2 here removes the last trailing ,
  ;;(print (.. "SELECT " (selection:sub 1 -2) " FROM " sqltbl " WHERE " filter ";"))
  (let
      [stmt (db:prepare (.. "SELECT " (selection:sub 1 -2) " FROM " sqltbl " WHERE " filter ";"))
       retval []]
    (each [row (stmt:nrows)]
      (table.insert retval row))
    (stmt:finalize)
    retval))

;;===== post =====
;;(post/add "title" "desc" "2022-01-01")
(fn post/add [title desc pubdate]
  "Add a post"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))
       stmt (db:prepare "INSERT into posts (title,desc,pubdate) VALUES (:title, :desc, :pubdate)")]
    (stmt:bind_names {:title title :desc desc :pubdate pubdate})
    (stmt:step)
    (stmt:finalize))
  (print (.. "{:Post " title "}")))

(fn post/edit [id]
  "Edit a post"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * from posts WHERE id = '" id "';"))]
      (let
          [post {"title" (. row "title")
                 "desc" (. row "desc")
                 "pubdate" (. row "pubdate")}
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect post)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "posts" ["title" "desc" "pubdate"] diff)))
    (db:close)))

(fn post/list []
  "Briefly list all posts"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows "SELECT * FROM posts;")]
      (print (.. "{:Id " (. row "id") " :Title " (. row "title") "}")))
    (db:close)))

;;===== api =====
(fn api/generate [user cred]
  "Generate an API key"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))
       stmt (db:prepare "INSERT into apikeys (key,user) VALUES (:key, :user)")
       key (util/encrypt cred)
       base (mime.b64 (util/encrypt (.. user ":" key)))]
    (stmt:bind_names {:key key :user user})
    (stmt:step)
    (stmt:finalize)
    (print (.. "{:Key " base "}"))))

(fn api/verify [base]
  "Validate API Key"
  (let
      [debase (util/split (util/decrypt (mime.unb64 base)) ":")
       db (sql.open (.. conf.lapis conf.db_loc))
       user (. debase 1)
       key (. debase 2)
       validation (sql/select db "apikeys" ["key"] "=" ["user"] [user])]
    (if (= key (. validation 1 "key"))
        (print "{:Valid true}")
        (print "{:Valid false}"))))

;;===== show =====
;;(show/add "name" "subtitle" "desc")
(fn show/add [name subtitle desc]
  "Add a podcast"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))
       stmt (db:prepare "INSERT into shows (name,subtitle,desc) VALUES (:name, :subtitle, :desc)")]
    (stmt:bind_names {:name name :subtitle subtitle :desc desc})
    (stmt:step)
    (stmt:finalize))
  (print (.. "{:Podcast " name "}")))

(fn show/edit [id]
  "Edit a podcasts info"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * from shows WHERE id = '" id "';"))]
      (let
          [pod {"name" (. row "name")
                "subtitle" (. row "subtitle")
                "desc" (. row "desc")}
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect pod)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "shows" ["name" "subtitle" "desc"] diff)))
    (db:close)))

(fn show/list [show]
  "Briefly list all podcasts"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * FROM shows;"))]
      (print (.. "{:Id " (. row "id") " :Name " (. row "name") "}")))
    (db:close)))

;;===== epi =====
;;(epi/add "show" "title" "desc" "2022-01-01")
(fn epi/add [show title desc pubdate]
  "Add a podcast episode"
  (var episode 1)
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))
       stmt (db:prepare "INSERT into episodes (show,episode,title,desc,pubdate) VALUES (:show, :episode, :title, :desc, :pubdate)")
       count (each [row (db:nrows (.. "SELECT count(episode) FROM episodes where show = '" show "';"))] (set episode (+ (. row "count(episode)") 1)))]
    (stmt:bind_names {:show show :episode episode :title title :desc desc :pubdate pubdate})
    (stmt:step)
    (stmt:finalize))
  (print (.. "{:Show " show " :Episode " title "}")))

(fn epi/edit [id]
  "Edit a podcast episode"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * from episodes WHERE id = '" id "';"))]
      (let
          [epi {"episode" (. row "episode")
                "show" (. row "show")
                "title" (. row "title")
                "desc" (. row "desc")
                "pubdate" (. row "pubdate")}
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect epi)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "episodes" ["episode" "show" "title" "desc" "pubdate"] diff)))
    (db:close)))

(fn epi/list [show]
  "Briefly list all podcasts in a show"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * FROM episodes where show = '" show "';"))]
      (print (.. "{:Id " (. row "id") " :Episode " (. row "episode") " :Title " (. row "title") "}")))
    (db:close)))

;;===== proj =====
;;(proj/add "name" "desc" "git")
(fn proj/add [name desc git]
  "Add a project"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))
       stmt (db:prepare "INSERT into projects (name,desc,git) VALUES (:name, :desc, :git)")]
    (stmt:bind_names {:name name :desc desc :git git})
    (stmt:step)
    (stmt:finalize))
  (print (.. "{:Project " name "}")))

(fn proj/edit [id]
  "Edit a projects info"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * from projects WHERE id = '" id "';"))]
      (let
          [proj {"name" (. row "name")
                "desc" (. row "subtitle")
                "id" (. row "desc")}
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect proj)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "projects" ["name" "desc" "git"] diff)))
    (db:close)))

(fn proj/list [show]
  "Briefly list all projects"
  (let
      [db (sql.open (.. conf.lapis conf.db_loc))]
    (each [row (db:nrows (.. "SELECT * FROM projects;"))]
      (print (.. "{:Id " (. row "id") " :Name " (. row "name") "}")))
    (db:close)))

;;===== rss =====
;;TODO: Function to generate an RSS feed from a post/episode select query table
;;Once that works add it as a post execution of the post/add & epi/add
;;Expose as a standalone function so a cronjob can be used to regenerate feeds daily as well (just in case)
;;Add an additional field to the RSS feed to capture the post text in the feed?

(fn rss/generate [feed show]
  (if (= feed "blog")
      (do
        (let
            [head "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
  <channel>
    <title>{title}</title>
    <link>{link}</link>
    <description>{desc}</description>
    <atom:link href=\"https://lambdacreate.com/static/feed.xml\" rel=\"self\" type=\"application/rss+xml\" />
"
             item "  <item>
      <title><![CDATA[{title}]]></title>
      <description><![CDATA[{desc}]]></description>
      <link>{link}{uri}</link>
      <guid>{link}{uri}</guid>
      <pubDate>{pubDate}</pubDate>
  </item>
"
             tail "  </channel>
</rss>"
             db (sql.open (.. conf.lapis conf.db_loc))]
          (var feed "")
          (set feed (util/template {1 head
                                    :link "https://lambdacreate.com"
                                    :desc "A blog held together entirely by lua, coffee, and crazy ideas."
                                    :title "(lambda (x) (create x))"}))
          (each [k v (pairs (sql/select db "posts" ["id" "title" "desc" "pubdate"] "LIKE" ["id"] ["%%"]))]
            (set feed (.. feed (util/template {1 item
                                               :title v.title
                                               :link "https://lambdacreate.com"
                                               :desc (with-open [f (io.open (.. conf.lapis "/views/posts/" v.id ".etlua") "r")]
                                                       (let [link "https://lambdacreate.com"]
                                                         (-> (f:read "*a")
                                                             (string.gsub "&middot" "&middot;")
                                                             (string.gsub "\"/static/images" (.. "\"" link "/static/images"))
                                                             (string.gsub "\"/posts/" (.. "\"" link "/posts/"))
                                                             (string.gsub "\"/podcast/" (.. "\"" link "/podcast/"))
                                                             (string.gsub "\"/archive/post/" (.. "\"" link "/archive/post"))
                                                             (string.gsub "\"/archive/podcast/" (.. "\"" link "/archive/podcast")))))
                                               :uri (.. "/posts/" v.id)
                                               :pubDate (util/rsstime v.pubdate)}))))
          (set feed (.. feed (util/template {1 tail})))
          (print feed)))
      (if (= feed "podcast")
          (let
              [head "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
<channel>
<title>{subtitle}</title>
<link>{link}</link>
<description>{desc}</description>
<atom:link rel=\"self\" type=\"application/rss+xml\" href=\"{link}{rss}\"/>
<image>
  <url>{link}{logo}</url>
  <title>{subtitle}</title>
  <link>{link}/podcasts/{title}</link>
</image>
<generator>LCM</generator>
<lastBuildDate>{builddate}</lastBuildDate>
<atom:link href=\"{link}{rss}\" rel=\"self\" type=\"application/rss+xml\"/>
<webMaster>{email} ({contact})</webMaster>
<managingEditor>{email} ({contact})</managingEditor>
<copyright>{contact}</copyright>
<language>en</language>
"
               item "  <item>
    <title>{title}</title>
    <link>{link}/podcast/{show}/{episode}</link>
    <pubDate>{pubDate}</pubDate>
    <description><![CDATA[{desc}]]></description>
    <guid isPermaLink=\"false\">{guid}</guid>
    <enclosure url=\"{link}{mp3}\" length=\"{length}\" type=\"audio/x-m4a\"/>
  </item>
"
               tail "  </channel>
</rss>"
               db (sql.open (.. conf.lapis conf.db_loc))
               info (. (sql/select db "shows" ["name" "subtitle" "desc"] "=" ["name"] [show]) 1)]
            (var feed "")
            (set feed (util/template {1 head
                                      :subtitle info.subtitle
                                      :title info.name
                                      :link "https://lambdacreate.com"
                                      :desc info.desc
                                      :rss (.. "/static/" info.name "/feed.xml")
                                      :logo (.. "/static/images/" info.name "/logo.png")
                                      :builddate (util/rsstime)
                                      :contact "Will Sinatra"
                                      :email "wpsinatra@gmail.com"}))
            (each [k v (pairs (sql/select db "episodes" ["id" "episode" "title" "desc" "pubdate" "show"] "=" ["show"] [show]))]
              (set feed (.. feed (util/template {1 item
                                                 :title v.title
                                                 :show v.show
                                                 :episode v.id
                                                 :link "https://lambdacreate.com"
                                                 :pubDate (util/rsstime v.pubdate)
                                                 :desc (with-open [f (io.open (.. conf.lapis "/views/" v.show "/" v.id ".etlua"))]
                                                         (let [link "https://lambdacreate.com"]
                                                           (-> (f:read "*a")
                                                               (string.gsub "&middot" "&middot;")
                                                               (string.gsub "<%% render%(\"views.audio\"%) %%>" "<p>Imagine an embedded media player, right here. Pretty ain't it?</p>")
                                                               (string.gsub "\"/static/images" (.. "\"" link "/static/images"))
                                                               (string.gsub "\"/posts/" (.. "\"" link "/posts/"))
                                                               (string.gsub "\"/podcast/" (.. "\"" link "/podcast/"))
                                                               (string.gsub "\"/archive/post/" (.. "\"" link "/archive/post"))
                                                               (string.gsub "\"/archive/podcast/" (.. "\"" link "/archive/podcast")))))
                                                 :guid (.. v.show "-" v.episode)
                                                 :mp3 (.. "/static/" v.show "/" v.show "-" v.episode ".mp3")
                                                 :length (util/sizeof (.. conf.lapis "/static/" v.show "/" v.show "-" v.episode ".mp3"))}))))
            (set feed (.. feed (util/template {1 tail})))
            (print feed)))))

;;===== main =====
(fn main [...]
  (util/conf (.. (os.getenv "HOME") "/.lcm.conf"))
  (let
      [parser (arg "lcm" "Lambda Create Management Tool")
       post (parser:command "post")
       show (parser:command "show")
       epi (parser:command "epi")
       proj (parser:command "proj")
       api (parser:command "api")
       rss (parser:command "rss")]
    ;;(parser:usage_margin 2)
    ;;(parser:help_usage_margin 3)
    ;;(parser:help_description_margin 40)
    ;;(parser:usage_max_width 80)
    ;;(parser:help_max_width 500)
    ;;=== post arguments
    (: (: (: (post:option "-a")
             :description "Add a post")
          :args 3)
       :argname ["title" "desc" "pubdate"])
    (: (post:flag "-s")
       :description "Show all posts")
    (: (: (: (post:option "-e")
             :description "Edit a post")
          :args 1)
       :argname ["#"])
        ;;=== proj arguments
    (: (: (: (proj:option "-a")
             :description "Add a project")
          :args 3)
       :argname ["name" "desc" "git"])
    (: (proj:flag "-s")
       :description "Show all projects")
    (: (: (: (proj:option "-e")
             :description "Edit a projects info")
          :args 1)
       :argname ["#"])
    ;;=== show arguments
    (: (: (: (show:option "-a")
             :description "Add a podcast")
          :args 3)
       :argname ["name" "subtitle" "desc"])
    (: (show:flag "-s")
       :description "Show all podcasts")
    (: (: (: (show:option "-e")
             :description "Edit a podcasts info")
          :args 1)
       :argname ["#"])
    ;;=== epi arguments
    (: (: (: (epi:option "-a")
             :description "Add an episode\nshow epi_title epi_desc date")
          :args 4)
       :argname ["show" "title" "desc" "pubdate"])
    (: (: (: (epi:option "-s")
             :description "Show all podcast episodes")
          :args 1)
       :argname "show")
    (: (: (: (epi:option "-e")
             :description "Edit an episode entry")
          :args 1)
       :argname ["#"])
    ;;=== api arguments
    (: (: (: (api:option "-i")
             :description "Issue an api key to a user\n$(pwmake 256 | head -c 15)")
          :args 2)
       :argname ["user" "cred"])
    (: (: (: (api:option "-v")
             :description "Verify an api key is valid")
          :args 1)
       :argname ["apikey"])
    ;;=== rss arguments
    (: (rss:flag "-b")
       :description "Generate a blog post feed")
    (: (: (: (rss:option "-p")
             :description "Generate a podcast feed")
          :args 1)
       :argname ["show"])
    ;;=== Arg handling
    (let
        [args (parser:parse)]
      ;;(print (inspect args))
      (if (. args "post")
          (if (. args "a")
              (post/add (. args "a" 1) (. args "a" 2) (. args "a" 3))
              (. args "s")
              (post/list)
              (. args "e")
              (post/edit (. args "e")))
          (. args "proj")
          (if (. args "a")
              (proj/add (. args "a" 1) (. args "a" 2) (. args "a" 3))
              (. args "s")
              (proj/list)
              (. args "e")
              (proj/edit (. args "e")))
          (. args "show")
          (if (. args "a")
              (show/add (. args "a" 1) (. args "a" 2) (. args "a" 3))
              (. args "s")
              (show/list)
              (. args "e")
              (show/edit (. args "e")))
          (. args "epi")
          (if (. args "a")
              (epi/add (. args "a" 1) (. args "a" 2) (. args "a" 3) (. args "a" 4))
              (. args "s")
              (epi/list (. args "s"))
              (. args "e")
              (epi/edit (. args "e")))
          (. args "api")
          (if (. args "i")
              (api/generate (. args "i" 1) (. args "i" 2))
              (. args "v")
              (api/verify (. args "v")))
          (. args "rss")
          (if (. args "b")
              (rss/generate "blog")
              (. args "p")
              (rss/generate "podcast" (. args "p")))))))

(main ...)
