#!/bin/ash
url=https://lambdacreate.com/paste
date=$(date +%s)
test_file=/tmp/test-$date.txt

if [ ! -f $test_file ]; then
	echo "This is a test" > $test_file
fi

curl -v -F key="$(pass show personal/api/lambdacreate)" -F upload=@$test_file $url
