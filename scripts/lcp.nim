import os, httpclient, mimetypes, strutils
#nim c -d:ssl -d:release lcp.nim

#Paste a file to lambdacreate.com/paste
proc lcp(): string =
  #Define httpclient and multipart data variables
  var
    client = newHttpClient()
    data = newMultipartData()
    content: string

  #Set lexical variables for configuration & auth
  let
    mimes = newMimetypes()            #  <- Instantiates mimetypes
    home = getEnv("HOME")             #  <- Grabs /home/username
    conf = home & "/.config/lcp.conf" #  <- Concats path to config file
    crypt = readFile(conf).strip()    #  <- Extract crpyt key for /paste auth

  #If we get more or less than 1 argument error
  if paramCount() < 1:
    return "A file must be specified for upload"

  if paramCount() > 1:
    return "Only one file is expected."

  #-F key="crypt"
  data["key"] = $crypt
  #-F upload=@file
  data.addFiles({"upload": paramStr(1)}, mimeDb = mimes)
  #http POST, strip new line on return

  try:
    let req = client.postContent("https://lambdacreate.com/paste", multipart=data)
    content = req.strip()
  except HttpRequestError:
    echo getCurrentException().msg
  finally:
    close(client)
  return content

echo lcp()
