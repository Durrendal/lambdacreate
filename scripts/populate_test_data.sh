#!/bin/ash

#Podcasts
./lcm.fnl show -a droidcast "A podcast on a droid" "Brief 15-20min segments recorded, produced, and published via the Droid4"
./lcm.fnl show -a lambdacast "Lambdacreate goes vocal!" "The same great lambdacreate content, in a brand new format."

#Podcast episodes
./lcm.fnl epi -a droidcast "Episode 1" "The first episode" 2022-01-01
./lcm.fnl epi -a droidcast "Episode 2" "The second episode" 2022-01-02
./lcm.fnl epi -a lambdacast "Episode 1" "The first episode" 2022-01-01
./lcm.fnl epi -a lambdacast "Episode 2" "The second episode" 2022-01-02

#API
./lcm.fnl api -i testing test > test.key

#Posts
./lcm.fnl post -a "Post 1" "The first post" 2022-01-01
./lcm.fnl post -a "Post 2" "The second post" 2022-01-02
./lcm.fnl post -a "Post 3" "The third post" 2022-01-03
./lcm.fnl post -a "Post 4" "The fourth post" 2022-01-04
./lcm.fnl post -a "Post 5" "The fifth post" 2022-01-05

#Projects
./lcm.fnl proj -a "tkts" "Super awesome cli ticket system" "https://gitlab.com/durrendal/tkts.git"

#RSS
./lcm.fnl rss -b > ../static/feed.rss
./lcm.fnl rss -p droidcast > ../static/droidcast/feed.rss
./lcm.fnl rss -p lambacast > ../static/lambdacast/feed.rss
