local config = require("lapis.config").config

config("development", {
  port = 8008,
  server = "cqueues",
  --server = nginx,
  code_cache = "off",
  num_workers = "1",
  access_log = "/tmp/logs/access.log",
  error_log = "/tmp/logs/error.log",
  sqlite = { database = "lc-dev.db" },
  cred = "lc.cred",
  site = "Lambdacreate Dev",
  debug = true,
  scheme = "http",
})

config("production", {
  port = 80,
  server = "nginx",
  code_cache = "on",
  num_workers = "4",
  access_log = "/var/log/nginx/access.log",
  error_log = "/var/log/nginx/error.log",
  sqlite = { database = "lc.db" },
  cred = "lc.cred",
  site = "Lambdacreate",
  debug = false,
  host = "lambdacreate.com",
  scheme = "https"
})

return config
