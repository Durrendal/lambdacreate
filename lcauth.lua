local lcauth = {}
local util = require("util")
local sql = require("lsqlite3")
local mime = require("mime")
local cipher = require("openssl.cipher")
local rand = require("openssl.rand")
local inspect = require("inspect")

function lcauth.validate(encstr)
   if encstr ~= nil then
   	local conf = util.conf("cipher.conf")
  	--Unbase Key, then decrypt
   	local base = mime.unb64(encstr)
   	local payload = util.decrypt(base, conf.type, conf.key)
   	--Split auth vals
   	local debase = util.split(payload, ":")
   	local user = debase[1]
   	local key = debase[2]
   	local db = sql.open("lc.db")
   	--Validate data against db
   	local validation = util.sql_select(db, "apikeys", {"key"}, "=", {"user"}, {user})

   	if key == validation[1].key then
	  	return true
   	else
	  	return false
   	end
   else
	return false
   end
end

return lcauth
