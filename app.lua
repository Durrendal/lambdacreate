local lapis = require("lapis")
local config = require("lapis.config").get()
local respond_to = require("lapis.application").respond_to
local to_json = require("lapis.util").to_json
local app = lapis.Application()
--LC Internals
local lcpaste = require("lcpaste")
local lcauth = require("lcauth")
local lcpod = require("lcpod")
local lcpost = require("lcpost")
local lcproj = require("lcproj")
local inspect = require("inspect")

--Enable view rendering
app:enable("etlua")

--debugging console
--local console = require("lapis.console")
--app:match("/console", console.make())

--Under construction
--app:match("construction", "/under_construction", function(self)
--			 self.timestamp = { updated_on="2022-07-03 17:15:00 +000" }
--			 self.internal_layout = "under_construction"
--			 return { render = true, layout = "layout" }
--end)

--Suppress error reporting
function app:handle_error(err, trace)
   return { redirect_to = self:build_url("e404", { scheme = config.scheme, host = config.host }) }
end

--Redirect HTTP 404 errors to funny templates.
app:match("e404", "/e404", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 self.internal_layout = "e404"
			 self.timestamp = {}
	     return { render = true, layout = "layout" }
end)

--Redirect HTTP 500 errors to funny templates.
app:match("e500", "/e500", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 self.internal_layout = "e500"
			 self.timestamp = {}
	     return { render = true, layout = "layout" }
end)

--Homepage
app:match("index", "/", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 --Table of last 10 blog posts
			 self.archive = lcpost.getLast(10)
			 --The last published post
			 self.latest = lcpost.getPost(lcpost.getLatest())
			 --Last update of published post
			 self.timestamp = lcpost.getUpdated(lcpost.getLatest())
			 self.shows = lcpod.getShows()
			 --Table of projects
			 self.projects = lcproj.getArchive()
			 self.internal_layout = "index"
			 return { render = true, layout = "layout" }
end)

--Blog Posts
app:match("/posts/:key", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 --Table of last 10 posts
			 self.archive = lcpost.getLast(10)
			 --Last update timestamp on current post
			 self.timestamp = lcpost.getUpdated(self.params.key)
			 self.internal_layout = "post"
			 return { render = true, layout = "layout" }
end)

--Blog posts/Podcast episode/paste archive lists
app:match("/archive/:type(/:show)", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 if self.params.type == "post" then
				--Table of all posts
				self.archive = lcpost.getArchive()
				self.timestamp = {}
				--For some reason this didn't work?
				--self.timestamp = {updated_on = (os.date "%a, %d %b %Y %H:%M:%S +000" (os.time))}
				self.internal_layout = "post_archive"
				return { render = true, layout = "layout" }
			 elseif self.params.type == "podcast" then
				--Specified show information
				self.show = lcpod.getShow(self.params.show)
				--Table of all episodes in the show
				self.archive = lcpod.getArchive(self.params.show)
				self.timestamp = {}
				--self.timestamp = {updated_on=(os.date "%a, %d %b %Y %H:%M:%S +000" (os.time))}
				self.internal_layout = "podcast_archive"
				return { render = true, layout = "layout" }
			 elseif self.params.type == "paste" then
				--Table of all things currently pasted
				self.archive = lcpaste.getArchive()
				self.timestamp = {}
				--self.timestamp = {updated_on=(os.date "%a, %d %b %Y %H:%M:%S +000" (os.time))}
				self.internal_layout = "paste_archive"
				return { render = true, layout = "layout" }
			 else
				--Redirect to e404 if the archive type doesn't exist
				return { redirect_to = self:url_for("404") }
			 end
end)

--About Info
app:match("info", "/info", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 self.timestamp = {}
			 self.internal_layout = "info"
			 return { render = true, layout = "layout" }
end)

--Project Posts
app:match("/dev/:name", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 self.project = lcproj.getProj(self.params.name)
			 self.projects = lcproj.getArchive()
			 self.timestamp = lcproj.getUpdated(self.params.name)
			 self.internal_layout = "project"
			 return { render = true, layout = "layout" }
end)

--Podcasts, /:showname gives show info /:showname/:episode gives episode info
app:match("/podcast/:show(/:episode)", function(self)
			 self.site = config.host
			 self.enc = config.scheme
			 --Only advertise these podcasts
			 --TODO convert to lcpod function to populate valid podcasts
			 if self.params.show == "lowtechradiogazette" then
				return { redirect_to = self:build_url("/podcast/ltrg", { scheme = config.scheme, host = config.host }) }
			 elseif self.params.show == "thelambdarecord" then
				return { redirect_to = self:build_url("/podcast/tlr") }
			 end
			 if self.params.show == "ltrg" or self.params.show == "tlr" then
				if self.params.episode == nil then
				   --Populate latest episode at show info page
				   self.show = lcpod.getShow(self.params.show)
				   self.episode = lcpod.getEpisode(self.params.show, lcpod.getLatest(self.params.show))
				   self.archive = lcpod.getLast(10, self.params.show)
				   self.timestamp = lcpod.getUpdated(self.params.show, lcpod.getLatest(self.params.show))
				   self.internal_layout = "podcast"
				   return { render = true, layout = "layout" }
				else
				   --Populate episode specified
				   self.show = lcpod.getShow(self.params.show)
				   self.episode = lcpod.getEpisode(self.params.show, self.params.episode)
				   self.archive = lcpod.getLast(10, self.params.show)
				   self.timestamp = lcpod.getUpdated(self.params.show, self.params.episode)
				   self.internal_layout = "podcast"
				   return { render = true, layout = "layout" }
				end
			 else
				--Send to 404 if podcast is invalid
				return { redirect_to = self:url_for("404") }
			 end
end)

--Paste Service
--curl -v -F key="key" -F upload=@argtest.fnl http://127.0.0.1:8080/paste
app:match("paste", "/paste(/:file)", respond_to({
				GET = function(self)
				   self.site = config.host
				   self.enc = config.scheme
				   --This GET allows us to share the same namespace as our POST
				   --static/paste      - nginx:nginx 0755
				   --static/paste/file - nginx:nginx 0640
				   return
				end,
				POST = function(self)
				   self.site = config.host
				   self.enc = config.scheme
				   --Check authorization of POST
				   local authn = lcauth.validate(self.params.key)
				   if authn == true then
					  --Upload a file to paste directory
					  local tmp = lcpaste.save(self.params.upload.content, self.params.upload.filename)
					  --Return the paste url
					  return {
						 render = false,
						 layout = false,
						 self:build_url({ scheme = config.scheme, host = config.host }) .. "/paste/" .. tmp .. "\n"
					  }
				   else
					  --Return access denied
					  return {
						 render = false,
						 layout = false,
						 "Access Denied\n"
					  }
				   end
				end,
}))

return app
